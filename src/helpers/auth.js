const auth = {
  setAuthToken: function (token) {
    localStorage.setItem('user-token', token)
  },
  getAuthToken: function () {
    return localStorage.getItem('user-token') || ''
  },
  setBatchId: function (batchId) {
    localStorage.setItem('batch-id', batchId)
  },
  getBatchId: function () {
    return localStorage.getItem('batch-id')
  }
}
auth.isAuth = auth.getAuthToken().length
auth.batchId = auth.getBatchId()

export default auth
