import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import './plugins/axios'
import VueChatScroll from 'vue-chat-scroll'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(VueChatScroll)
Vue.prototype.noticePath = 'http://pgdit.local/storage/notices/'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
