export default {
  reducePrice: (context, payload) => {
    setTimeout(() => {
      context.commit('reducePrice', payload)
    }, 2000)
  }
}
