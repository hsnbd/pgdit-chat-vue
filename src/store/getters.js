export default {
  /* saleProduct: state => {
    return state.products.map(item => {
      return {
        name: '--' + item.name + '--',
        price: item.price * 2
      }
    })
  } */
  getAuthUser: state => state.authUser,
  getBatchId: state => state.authUser.batch_id,
  authToken: state => state.accessToken,
  isAuth: state => state.isAuthenticated
}
