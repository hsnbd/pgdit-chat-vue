import auth from '../helpers/auth'

export default {
  /* reducePrice: (state, payload) => {
    state.products.forEach(item => {
      item.price -= payload
    })
  } */
  setAuthUser: (state, payload) => {
    state.authUser = payload
  },
  setAuthToken: (state, payload) => {
    auth.setAuthToken(payload)
    state.accessToken = payload
  },
  setIsAuth: (state, payload) => {
    state.isAuth = payload
  }
}
