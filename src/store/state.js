import auth from '../helpers/auth'

export default {
  messages: [
    { name: 'Hasan', id: 1, content: 'abdfdfdfd' },
    { name: 'Jaman', id: 2, content: 'fsdfdsfsdsf' },
    { name: 'Hasan', id: 3, content: 'sfdsfsdfdf' },
    { name: 'Hasan', id: 4, content: 'sfdsfdsfsd' }
  ],
  authUser: { id: null, batch_id: null, name: null, email: null, gender: null, image: null },
  accessToken: auth.getAuthToken(),
  isAuthenticated: auth.isAuth > 10
}
